import sys
import h5py
from model import BasicModel
import pickle
import os

if __name__ == "__main__":
    assert(len(sys.argv) == 2)
    model = BasicModel("./model_parameters.pickle")
    print("parameters", model.parameters)
    model.predict(sys.argv[1])

