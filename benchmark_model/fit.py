import sys
import h5py
from model import BasicModel

if __name__ == "__main__":
    assert(len(sys.argv) == 2)
    model = BasicModel()
    model.fit(sys.argv[1])
    model.safe("./")
