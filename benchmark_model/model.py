import sys
sys.path.insert(0,'..')
from common import Model
import h5py
import numpy as np
import pandas as pd
import pickle

class BasicModel(Model):
    def get_score(self, y_hat, y):
        return 10000 * ((np.array(y_hat) - np.array(y))**2).sum() / y_hat.shape[0]
    
    # return np.array of same length as X
    def get_prediction(self, X, parameters):
        a = pd.DataFrame(index = X["TS"], data = 0.5 * (X["Ask"][:, 0] + X["Bid"][:, 0]), columns = ["mid_price"])
        a["prediction"] = a.rolling(window = parameters["window"], min_periods = 0).mean()
        a["prediction"] /= a["mid_price"]
        a["prediction"] -= 1
        return a["prediction"]
        

    def fit(self, path_to_folder):
        window_values = np.arange(1, 122, 10)
        best_score = 1e8
        with h5py.File(path_to_folder+"/data.h5", "r") as data:
            with h5py.File(path_to_folder + "/result.h5", "r") as res:
                X = data["OB"]
                y = res["Return"]["Res"]
                for window in window_values:
                    parameters = {}
                    parameters["window"] = window
                    prediction = self.get_prediction(X, parameters)
                    score = self.get_score(prediction, y)
                    print("current: ", parameters["window"], score)
                    if(score < best_score):
                        print(score, parameters["window"])
                        best_score = score
                        self.parameters = parameters
        print("best score: ", best_score)
        print("parameters: ", self.parameters)
    
    def predict(self, path_to_data):
        with h5py.File(path_to_data, "r") as data:
            with h5py.File("prediction.h5", "w") as pred:
                X = data["OB"]
                pred.create_group("Return")
                
                pred["Return"].create_dataset("TS", data=X["TS"])
                prediction = self.get_prediction(X, self.parameters)
                pred["Return"].create_dataset("Res", data = prediction)
                print("finished")
    
