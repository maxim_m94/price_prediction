import sys
sys.path.insert(0,'..')
import os
from common import Model, CustomGen
import h5py
import numpy as np
import pandas as pd
import pickle
import tensorflow as tf
import keras
from keras import optimizers, initializers
from keras.layers import Dense, MaxPooling2D, Dropout, Flatten, Conv2D, Conv1D
from keras import regularizers

import shutil

class OB_model(Model):
    def __init__(self, path_to_params=None, path_to_weights=None, params=None):
        super().__init__(path_to_params, path_to_weights)
        if(path_to_params == None):
            self.params = params
            self.init_model()
            self.fitted = False
            
    
    def init_model(self):
        print("\t initing model")
        self.predictor = keras.Sequential()
        w = self.params["ob_window"]
        input_shape = (1, 25, 2)#(w, 4*self.params["depth"], 1)
        # self.predictor.add(Dense(10, activation='relu', input_shape=(w, 2*self.params["depth"], 2)))
        # self.predictor.add(Conv2D(w//4, kernel_size = 3, activation='relu', input_shape=input_shape))
        # self.predictor.add(MaxPooling2D(pool_size=(1, 2)))
        # self.predictor.add(Conv2D(w//2, kernel_size = 3, activation='relu'))
        # self.predictor.add(MaxPooling2D(pool_size=(1,2)))
        # self.predictor.add(Dropout(0.1))
        # self.predictor.add(Conv2D(w, kernel_size = 3, activation = 'relu'))
        # self.predictor.add(MaxPooling2D(pool_size=(2,2)))
        # self.predictor.add(Dropout(0.1))
        # self.predictor.add(Conv2D(w, kernel_size = 3, activation = 'relu'))
        # self.predictor.add(MaxPooling2D(pool_size=(2,2)))
        # self.predictor.add(Dropout(0.1))

        #self.predictor.add(Conv2D(w*16, kernel_size = 3, activation = 'relu'))
        #self.predictor.add(MaxPooling2D(pool_size=(2,1)))
        #self.predictor.add(Dropout(0.1))
        self.predictor.add(Flatten(input_shape=input_shape))
        # self.predictor.add(Dense(50, activation='relu'))
        # self.predictor.add(Dropout(0.1))
        #self.predictor.add(Dense(50, activation='relu'))
        #self.predictor.add(Dense(100, activation='relu'))
        self.predictor.add(Dense(1, activation='linear'))
        self.predictor.compile(optimizer=optimizers.RMSprop(lr=0.001), loss='mean_squared_error')
        print(self.predictor.summary())

    def init_generators(self, data, y_data):
        print("\t initing generators")

        X_data = data["data"]
        length = X_data.shape[0]
        border = int(length * self.params["train_share"])
        
        self.training_generator = TimeseriesGenerator(data=X_data[0:border], 
                                            targets=np.concatenate([[0],y_data[0:border-1]]),
                                            length=self.params["ob_window"],
                                            batch_size=self.params["batch_size"],)
                                            #BidAsk=data["BidAsk"][0:border])
        self.validation_generator = TimeseriesGenerator(data=X_data[border:length], 
                                            targets=np.concatenate([[0], y_data[border:length-1]]),
                                            length=self.params["ob_window"],
                                            batch_size=self.params["batch_size"],)
                                            #BidAsk=data["BidAsk"][border:length])

    def fit(self, data, y, path_to_safe_models='./model_checkpoints/'):
        if(os.path.exists(path_to_safe_models)):
            shutil.rmtree(path_to_safe_models)
        os.makedirs(path_to_safe_models)
        print("init_predictor... \n")
        self.init_generators(data, y)
        print("start fitting... \n")
        callback = [keras.callbacks.ModelCheckpoint(filepath=path_to_safe_models + '/model.{epoch:02d}-{val_loss:.2f}.h5'),]
        self.params["history"] = self.predictor.fit(self.training_generator, 
                                                    validation_data=self.validation_generator, 
                                                    workers=7, 
                                                    use_multiprocessing=False,
                                                    callbacks=callback, 
                                                    epochs=self.params["n_epochs"])
        self.params["predictor"] = self.predictor
        self.fitted = True
        return self.params["history"]

    def predict(self, data, path_to_safe_pred):
        assert self.fitted
        with h5py.File(path_to_safe_pred, "w") as pred:
            l = data["data"].shape[0]//100
            w = self.params["ob_window"]
            test_generator = CustomGen(data=data["data"][:l], 
                                        targets=np.zeros(l),
                                        length=self.params["ob_window"],
                                        batch_size=self.params["batch_size"],
                                        BidAsk=data["BidAsk"][:l])
            pred.create_group("Return")
            pred["Return"].create_dataset("TS", data=data["TS"])
            my_predictions = self.params["predictor"].predict(test_generator, verbose=True)
            pred["Return"].create_dataset("Res", data=np.zeros(l))
            pred["Return"]["Res"][w:] = my_predictions.reshape(-1)
            print("finished")
    
