import sys
import h5py
from model import OB_model
import pickle
import os

if __name__ == "__main__":
    assert(len(sys.argv) == 3)
    model = OB_model("./model_parameters.pickle")
    print("parameters", model.parameters)
    model.predict(sys.argv[1], sys.argv[2])

