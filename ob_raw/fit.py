import sys
import h5py
from model import OB_model
from common import Preprocessor
import os

if __name__ == "__main__":
    assert(len(sys.argv) == 2)
    if not os.path.exists(sys.argv[1] + "/prep_data.h5"):
        print("doing data preprocessing... \n")
        p = Preprocessor(sys.argv[1])
        p.run()
    print("building model... \n")
    params={"ob_window": 10,
            "train_share": 0.6,
            "batch_size": 128,
            "depth": 15,
            "n_epochs": 1}
    model = OB_model(params=params)
    print("start fitting... \n")
    model.fit(sys.argv[1])
    model.safe("./")
