import h5py
import numpy as np
import sys

if __name__ == "__main__":
    assert len(sys.argv) == 3, "current is "+ str(len(sys.argv))
    with h5py.File(sys.argv[1], "r") as f1:
        with h5py.File(sys.argv[2], "r") as f2:
            r2 = np.array(f2["Return"]["Res"]).reshape(-1)
            r1 = np.array(f1["Return"]["Res"])[-r2.shape[0]:] * 1000
            
            print(r1.shape,r1[-10:])
            print(r2.shape, r2[10000:10100])
            assert(f1["Return"]["TS"][0] == f2["Return"]["TS"][0]), "timeperiods don't match"
            assert(r1.shape[0] == r1.shape[0])
            print(((r1 - r2)**2).sum() / r1.shape[0])

