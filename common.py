import pickle
import h5py
import numpy as np
import pandas as pd
import h5py
from sklearn.preprocessing import MinMaxScaler
from keras.preprocessing.sequence import TimeseriesGenerator
from multiprocessing import Pool
from sklearn.externals import joblib 

class Model:
    # initialise model from the file
    def __init__(self, path_to_params=None, path_to_model_weights=None):
        if(path_to_params == None):
            self.params={}
        else:
            with open(path_to_params, "rb") as par:
                self.params = pickle.load(par)
                self.params["predictor"].load_weights(path_to_model_weights)
                
            
    def init_model():
        pass

    # create model.pickle file at destination with model params
    def safe(self, path_to_folder):
        with open(path_to_folder + '/model_params.pickle', 'wb') as handle:
            pickle.dump(self.params, handle, protocol=pickle.HIGHEST_PROTOCOL)
    
    # fits model params
    def fit(self, path_to_folder):
        pass

    # returns path to forecast file
    def predict(self, path_to_data):
        pass

    def get_score(self, y_hat, y):
        return 10000 * ((np.array(y_hat) - np.array(y))**2).sum() / y_hat.shape[0]

class CustomGen(TimeseriesGenerator):
    def __init__(self, data, targets, length, batch_size, BidAsk):
        self.BidAsk = np.array(BidAsk, dtype=int)
        assert(data.shape[0] == BidAsk.shape[0])
        super().__init__(data=data, targets=targets, length=length, batch_size=batch_size)
    def __getitem__(self, ind):
        #print(ind)
        const_to_fill = 0.1
        X, y = super().__getitem__(ind)
        #print("X.shape", X.shape)
        #print("y[ind]", y)
        new_X = np.zeros((X.shape[0], X.shape[1], int(X.shape[2] * 2.5)))
        l = int(new_X.shape[2]//2)
        level_zero = int((self.BidAsk[self.length + ind, 1] + self.BidAsk[self.length + ind, 0])/2)
        for t in range(new_X.shape[0]):
            for i in range(new_X.shape[1]):
                ask_down = l +  self.BidAsk[ind +i, 1] - level_zero
                bid_up = l + self.BidAsk[ind + i, 0] - level_zero
                #print(l, self.BidAsk[ind +i, 1], self.BidAsk[ind + i, 0], level_zero)
                #print(ask_down, bid_up)
                j = 0
                while j < X.shape[2] and j + ask_down < new_X.shape[2]:
                    if j+ask_down >= 0:
                        new_X[t, i, j+ask_down] = X[t, i, j, 1]
                    j += 1
                if j + ask_down < new_X.shape[2]:
                    new_X[t, i, max(j+ask_down, 0):new_X.shape[2]] = const_to_fill

                j = 0
                while(j < X.shape[2] and bid_up-j >= 0):
                    if bid_up-j < new_X.shape[2]:
                        new_X[t, i, bid_up-j] = X[t, i, j, 0]
                    j += 1
                if(bid_up - j >= 0):
                    new_X[t, i, 0:min(bid_up-j,2*l)] = -const_to_fill
        return new_X.reshape(*new_X.shape, 1), y



class PicturePreprocessor:
    def __init__(self, path_to_folder, params):
        self.path_to_folder = path_to_folder
        self.params = params
        # params = {
        #     "length: 10"
        #     "batch_size" : 1000
        # }

    def run(self):
        params = self.params
        with h5py.File(self.path_to_folder + "/prep_data2.h5", "r") as data:
            with h5py.File(self.path_to_folder + "/prep_dataPIC.h5", "w") as data_pic:
                
                gen = CustomGen(data["data"], np.zeros(data["data"].shape[0]), 
                                length=params["length"],
                                batch_size=params["batch_size"],
                                BidAsk=data["BidAsk"])

                sample = gen[0][0]
                data_pic.create_dataset("data", shape=(data["data"].shape[0], *sample[0].shape))
                data_pic.create_dataset("TS", data = data["TS"])

                data_pic["data"][:params["length"]] = np.zeros(shape=(params["length"],*sample[0].shape))

                for i in range(len(gen)):
                    print (i,"out of", len(gen))
                    X, y = gen[i]
                    l = i*params["batch_size"]+params["length"]
                    r = l + X.shape[0]
                    data_pic["data"][l:r] = X
                    

class DecomposePreprocessor:
    def __init__(self, path_to_folder, params):
        self.path_to_folder = path_to_folder
        self.params = params
        # params = {
        #     "depth_levels" : 15, 
        #     "price_precision" : 0.5, 
        #     "quantity_max" : 15.0, 
        #     "n_batches" : 100
        # }

    def run(self):
        params = self.params
        with h5py.File(self.path_to_folder + "/data.h5", "r") as data:
            with h5py.File(self.path_to_folder + "/prep_data2.h5", "w") as prep_data:
                with h5py.File(self.path_to_folder + "/result.h5", "r") as targets:
                    l = data["OB"]["Ask"].shape[0]
                    prec = params["price_precision"]
                    levels = params["depth_levels"]
                    prep_data.create_dataset("data", shape=(l, levels, 2))
                    prep_data.create_dataset("BidAsk", shape=(l, 2), dtype=int)
                    prep_data.create_dataset("TS", data=data["OB"]["TS"])
                    steps = params["n_batches"]
                    batch = (l // steps)
                    
                    for j in range(steps + 1):
                        print(j)
                        left = batch * j
                        right = min(batch * (j+1), l)

                        ask_prices = (np.array(data["OB"]["Ask"][left:right]) / prec + (1 - prec)).astype(int)
                        prep_data["BidAsk"][left:right, 1] = ask_prices[:, 0].astype(int)
                        ask_prices -= ask_prices[:, 0].reshape(-1, 1)
                        ask_prices[ask_prices > levels-1] = levels-1
                        ask_volumes = np.array(data["OB"]["AskV"][left:right]) / params["quantity_max"]
                        ask_volumes[ask_volumes > 1] = 1

                        bid_prices = (np.array(data["OB"]["Bid"][left:right]) / prec).astype(int)
                        prep_data["BidAsk"][left:right, 0] = (bid_prices[:,0]).astype(int)
                        bid_prices -= bid_prices[:, 0].reshape(-1, 1)
                        bid_prices[bid_prices < -(levels-1)] = -(levels-1)
                        bid_prices *= -1
                        bid_volumes = np.array(data["OB"]["AskV"][left:right]) / params["quantity_max"]
                        bid_volumes[bid_volumes > 1] = 1
                        
                        ask = np.array([np.bincount(ask_prices[i], 
                                                    weights=ask_volumes[i], minlength=levels) for i in range(right-left)])
                        ask_res = np.ones((right-left, levels)) * 0.001
                        ask_res[:, :ask.shape[1]] += ask

                        bid = np.array([np.bincount(bid_prices[i], 
                                                    weights=bid_volumes[i], minlength=levels) for i in range(right-left)])
                        bid_res = np.ones((right-left, levels)) * -0.001
                        bid_res[:, :bid.shape[1]] -= bid
                        
                        prep_data["data"][left:right,:,0] = bid_res
                        prep_data["data"][left:right,:,1] = ask_res

class Preprocessor:
    def __init__(self, path_to_folder):
        self.path_to_folder = path_to_folder
        self.quantity_hardcode_max = 3.0
        self.price_hardcode_max = 10000.0
        self.price_hardcode_min = 5000.0
    
    def run(self):
        path = self.path_to_folder + "/data.h5"
        
        def handle_price(arr, is_ask):
            return (arr - self.price_hardcode_min) / (self.price_hardcode_max - self.price_hardcode_min)
        
        def handle_vol(arr):
            arr /= self.quantity_hardcode_max
            arr[arr > 1] = 1
            return arr

        with h5py.File(path, "r") as data:
            with h5py.File(self.path_to_folder + "/prep_data.h5", "w") as prep_data:
                l = data["OB"]["Ask"].shape[0]
                prep_data.create_dataset("data", shape=(l, 60, 2))
                batch = (l // 100)
                for i in range(101):
                    print(i)
                    left = batch * i
                    right = min(batch * (i+1), l)
                    ask_prices = handle_price(np.array(data["OB"]["Ask"][left:right]))
                    bid_prices = handle_price(np.array(data["OB"]["Bid"][left:right]))

                    ask_volumes = handle_vol(np.array(data["OB"]["AskV"][left:right]))
                    bid_volumes = handle_vol(np.array(data["OB"]["BidV"][left:right]))

                    ask_data = np.concatenate([ask_prices, ask_volumes], axis = 1)
                    bid_data = np.concatenate([bid_prices, bid_volumes], axis = 1)
                    res = np.concatenate([np.expand_dims(ask_data, axis=2), np.expand_dims(bid_data, axis=2)], axis = 2)
                    prep_data["data"][left:right] = res
            with h5py.File(self.path_to_folder + "/TS.h5", "w") as TS_file:
                TS_file.create_dataset("TS", data = data["OB"]["TS"])

class ExtFeatPrep:
    def __init__(self, path_to_folder, params, is_fit):
        self.path_to_folder = path_to_folder
        self.params = params
        self.is_fit = is_fit
    
    def get_linear_coeff(self, x, y):
        val = ((x*y).sum() - x.sum()*y.sum()/x.shape[0]) / ((x**2).sum() - (x.sum()**2)/x.shape[0])
        return val

    def run(self):
        params = self.params
        with h5py.File(self.path_to_folder + "/data.h5", "r") as data:
            with h5py.File(self.path_to_folder + "/prep_data3.h5", "w") as prep_data:
                l = data["OB"]["Ask"].shape[0]
                w = self.params["ob_window"]
                prep_data.create_dataset("TS", data=data["OB"]["TS"])
                mid_price = pd.DataFrame(data=(data["OB"]["Ask"][:, 0] + data["OB"]["Bid"][:, 0]) / 2, columns=["mid_price"])
                
                a = pd.DataFrame()
                self.scalers = {}
                self.scalers["linear"] = MinMaxScaler()
                a["linear_long"] = mid_price["mid_price"].rolling(window=w, min_periods=1).apply(lambda x: self.get_linear_coeff(np.arange(x.shape[0]), x))
                #a["linear_short"] = mid_price["mid_price"].rolling(window=int(w/2), min_periods=1).apply(lambda x: self.get_linear_coeff(np.arange(x.shape[0]), x))
                scaler = MinMaxScaler()
                a["from_max_long"] = mid_price["mid_price"].rolling(window=w, min_periods=1).max() / mid_price["mid_price"]
                a["from_max_short"] = mid_price["mid_price"].rolling(window=int(w/2), min_periods=1).max() / mid_price["mid_price"]
                a["from_min_long"] = mid_price["mid_price"].rolling(window=w, min_periods=1).min() / mid_price["mid_price"]
                a["from_min_short"] = mid_price["mid_price"].rolling(window=int(w/2), min_periods=1).min() / mid_price["mid_price"]
                a["from_mean_long"] = mid_price["mid_price"].rolling(window=2, min_periods=1).mean() / mid_price["mid_price"]
                a["from_mean_short"] = mid_price["mid_price"].rolling(window=int(w/2), min_periods=1).mean() / mid_price["mid_price"]
                a["std"] = mid_price["mid_price"].rolling(window=int(w/2), min_periods=1).std()
                a = a.fillna(0).values
                if self.is_fit:
                    scaler.fit(a)
                    joblib.dump(scaler, "scaler.pkl")
                    print("scaler fitted and dumped to ", "scaler.pkl")
                else:
                    scaler = joblib.load("scaler.pkl")
                    print("scaler loaded from ", "scaler.pkl")

                prep_data.create_dataset("data", data = scaler.transform(a))


    