import sys
sys.path.insert(0,'..')
from common import Model
import h5py
import numpy as np
import pandas as pd
import pickle
from Datagenerator import DataGenerator
import keras
from keras import optimizers
from keras.layers import Dense, Conv1D, MaxPooling1D, Dropout, Flatten
from keras import regularizers

# produces prediction based on previous trade prices and volumes
class Autoregression(Model):
    def __init__(self, path_to_params=None):
        super().__init__(path_to_params)
        self.parameters["ob_window"] = 40 # number of OB in the past
        self.l = 0.8
        self.batch_size = 5000
    
    def init_predictor(self, path_to_folder):
        with h5py.File(path_to_folder + "/data.h5", "r") as data:
            length = data["OB"]["Ask"].shape[0]
            border = int(length * self.l)
            w = self.parameters["ob_window"]
            self.training_generator = DataGenerator(0, border, path_to_folder, w, batch_size=self.batch_size)
            self.validation_generator = DataGenerator(border, length, path_to_folder, w, batch_size=self.batch_size)
            
            self.predictor = keras.Sequential()
            self.predictor.add(Conv1D(w//2, kernel_size = 5, activation='relu', input_shape=(w, 1)))
            self.predictor.add(Conv1D(w//4, kernel_size = 3, activation = 'relu'))
            self.predictor.add(Dropout(0.1))
            self.predictor.add(Conv1D(w//8, kernel_size = 3, activation = 'relu'))
            self.predictor.add(MaxPooling1D())
            self.predictor.add(Dropout(0.1))
            self.predictor.add(Flatten())
            self.predictor.add(Dense(5, activation='relu'))
            self.predictor.add(Dense(1, activation='linear'))
            self.predictor.compile(optimizer=optimizers.Adam(lr=0.0001), loss='mean_squared_error')
            print(self.predictor.summary())

    def fit(self, path_to_folder):
        print("init_predictor... \n")
        self.init_predictor(path_to_folder)
        print("start fitting... \n")
        self.predictor.fit(self.training_generator, 
                            validation_data=self.validation_generator, 
                            workers=8, 
                            use_multiprocessing=False, 
                            epochs = 5)
        self.parameters["predictor"] = self.predictor

    def predict(self, path_to_folder):
        with h5py.File(path_to_folder + "/data.h5", "r") as data:
            with h5py.File("prediction.h5", "w") as pred:
                with open("model_parameters.pickle", "rb") as params:
                    p = pickle.load(params)
                    l = data["OB"]["Ask"].shape[0]
                    w = p["ob_window"]
                    test_generator = DataGenerator(0, l, path_to_folder, w, batch_size=100000, to_fit=False)
                    pred.create_group("Return")
                    pred["Return"].create_dataset("TS", data=data["OB"]["TS"])
                    pred["Return"].create_dataset("Res", data=np.zeros(data["OB"]["TS"].shape[0]))
                    print("start predicting... \n")
                    for i in range(len(test_generator)):
                        cur_range = test_generator.get_range(i)
                        pred["Return"]["Res"][cur_range[0] : cur_range[1]] = p["predictor"].predict_on_batch(test_generator[i]).reshape(-1)

                    print("last predictions: ", pred["Return"]["Res"][-20:], "\n")
                    print("finished")
    
