import sys
import h5py
from model import Autoregression

if __name__ == "__main__":
    assert(len(sys.argv) == 2)
    model = Autoregression()
    model.fit(sys.argv[1])
    model.safe("./")
