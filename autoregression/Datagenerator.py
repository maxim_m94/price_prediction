
import numpy as np
from keras.utils import Sequence
import h5py
import pandas as pd

class DataGenerator(Sequence):
    def __init__(self, index_from, index_to, path_to_folder, ob_window,
                 to_fit=True, batch_size=32):

        self.index_from = max(index_from, ob_window)
        self.index_to = index_to
        self.path_to_folder = path_to_folder #if to_fit==False, can be a path straight to a data file
        self.ob_window = ob_window
        self.to_fit = to_fit
        self.batch_size = batch_size
        self.quantity_hardcode_max = 3.0
        self.internal_index = 0

    def __len__(self):
        return int(np.floor((self.index_to - self.index_from)/self.batch_size + 0.0001))

    def get_range(self, index):
        return (self.index_from + index * self.batch_size, self.index_from + (index + 1) * self.batch_size)

    def __getitem__(self, index):
        if(index%100 == 0):
            print(index * self.batch_size)
        # Generate indexes of the batch
        #print("from index to", self.index_from, self.index_from + index * self.batch_size, self.index_to)
        cur_indexes = self.get_range(index)

        # Generate data
        X = self._generate_X(cur_indexes)

        if self.to_fit:
            y = self._generate_y(cur_indexes)
            return X, y
        else:
            return X

    def bin_search(arr, value):
        l = 0
        r = arr.shape[0]
        while(r - l > 1):
            cur = (l + r) // 2
            if(arr[cur] > value):
                r = cur
            else:
                l = cur
        return r

    def _generate_X(self, cur_indexes):
        if(self.path_to_folder[-3:0] != ".h5"):
            path = self.path_to_folder + "/data.h5"
        else:
            path = self.path_to_folder
        
        with h5py.File(path, "r") as data:
            l = cur_indexes[0] - self.ob_window
            assert(l >= 0)
            r = cur_indexes[1]
            #print("cur_indees", cur_indexes)
            cur_mid_prices = (np.array(data["OB"]["Ask"][l:r, 0]) + 
                            np.array(data["OB"]["Bid"][l:r, 0]))/2
            #print("mid_price shape ", cur_mid_prices.shape, "\n")
            #print("mid_price_head\n",cur_mid_prices[:5], "\n")
            a = pd.DataFrame(data = cur_mid_prices, columns=[0])
            #assert(False)
            for i in range(1, self.ob_window): 
                a[i] = a[i-1].shift()
            a = a.apply(lambda x: x/a[0])[self.ob_window:]
            #print("x",cur_indexes, a.shape)
            #print(a.head())
            return np.expand_dims(a.values, axis=2)

    def _generate_y(self, cur_indexes):
         with h5py.File(self.path_to_folder + "/result.h5", "r") as result:
             a = result["Return"]["Res"][cur_indexes[0]:cur_indexes[1]]
             #print("y", cur_indexes, a.shape)
             return a